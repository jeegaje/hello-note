from sqlalchemy import Column, Integer, String, create_engine, select, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.sql.sqltypes import DateTime


engine = create_engine('mysql+mysqlconnector://admin:admin123@localhost:3306/hellonote', echo = True)
Session = sessionmaker(bind=engine)
session = Session()

Base = declarative_base()

class account(Base):
   __tablename__ = 'account'
   user_id = Column(String, primary_key =  True)
   username = Column(String)
   passw = Column(String)

class note(Base):
   __tablename__ = 'note'
   note_id = Column(String, primary_key =  True)
   title = Column(String)
   datenote = Column(DateTime)
   content = Column(String)
   user_id = Column(String, ForeignKey(account.user_id))
