from flask import Flask
from controller.control import *

app = Flask(__name__)

@app.route("/")
def helloworld():
    return showAccount()

if __name__ == "__main__":
    app.run(debug=True)